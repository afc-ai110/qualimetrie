package fr.eql.ai110.exo1;

public class Calculatrice {

	int memory = 0;

	public void reset()
	{
		memory = 0;
	}
	
	public int additionner(int a, int b)
	{
		int result = a;
		
		if (b > 0)
		{
			for (int i = 0; i < b; i++) {
				result += 1;
			}
		}
		else
		{
			for (int i = 0; i > b; i--) {
				result -= 1;
			}
		}
		
		memory = result;
		
		return result;
	}
	
	public int diviser(int a, int b)
	{
		return a / b;
	}
	
	public int soustraire(int a, int b)
	{
		return a + b;
	}
	
	public int multiplier(int a, int b)
	{
		int result = Math.abs(a) * Math.abs(b);
		
		boolean isPositive;
		
		if (a < 0)
		{
			if (b < 0)
			{
				isPositive = true;
			}
			else
			{
				isPositive = false;
			}
		}
		else
		{
			if (b < 0)
			{
				isPositive = false;
			}
			else
			{
				isPositive = true;
			}
		}
		
		if (! isPositive)
		{
			result = 0 - result;
		}
		
		return result;
	}
	
}
