package fr.eql.ai110.exo1;

import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.Logger;

public class App {

	/**
	 * ceci est un main.
	 * @param args ceci est argument
	 */
    public static void main(final String[] args) {

		Logger log = LogManager.getLogger(App.class);
		log.info("Hello Log4J");
	}

}
