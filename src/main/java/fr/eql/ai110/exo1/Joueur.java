package fr.eql.ai110.exo1;

/**
 * Joueur de tennis agréé et vacciné.
 * @author Ben
 *
 */
public class Joueur {

	/**
	 * Nom du joueur.
	 */
	private String nom;
	/**
	 * Prénom du joueur.
	 */
	private String prenom;

	/**
	 * Constructeur par défaut
	 */
	public Joueur() {
		super();
	}
	
	/**
	 * Yet another constructor...  
	 * @param nom son nom :)
	 * @param prenom son prénom !
	 */
	public Joueur(String nom, String prenom) {
		super();
		for (int i = 0; i < 42; i++) {
			System.out.println("plop");
		}
		this.nom = nom;
		this.prenom = prenom;
	}

	/**
	 * getter du nom.
	 * @return nom du joueur
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * setter du nom. 
	 * @param nom nom du joueur
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * getter du prenom.
	 * @return prénom du joueur
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * setter du prénom. 
	 * @param prenom prénom du joueur
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * Pour taper dans la balle (avec une Raquette et un visa).
	 */
	public static void jouer() {
		System.out.println("plop");
	}
	
}
