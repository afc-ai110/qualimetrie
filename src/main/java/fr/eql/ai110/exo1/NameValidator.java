package fr.eql.ai110.exo1;

import java.util.function.BooleanSupplier;

public class NameValidator {

	private static final int MINIMUM_WORDS_COUNT = 2;

	public boolean isCorrectFullname(String fullname) {
		boolean isCorrect = false;
		String[] words = fullname.split(" ");
		if (fullnameContainsAtLeastMinimumWordCount(words) &&
			allWordsAreEitherUpperOrCapital(words) &&
			atLeastOneWordIsUppercase(words) &&
			atLeastOneWordIsCapitalcase(words) &&
			capitalsAndUpperWordsAreGrouped(words)) {
			isCorrect = true;
		}
		return isCorrect;
	}

	private boolean capitalsAndUpperWordsAreGrouped(String[] words)
	{
		boolean startsWithUppercaseWord = isUpperCase(words[0]);
		int section = 1;
		for (int i = 1; i < words.length; i++) {
			String currentWord = words[i];
			if (section == 1 && isUpperCase(currentWord) == !startsWithUppercaseWord) {
				section = 2;
			}
			
			if (section == 2)
			{
				if (isUpperCase(currentWord) == startsWithUppercaseWord)
				{
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean allWordsAreEitherUpperOrCapital(String[] words) {
		for (String word: words) {
			if (! isUpperCase(word) && !isCapitalCase(word)) {
				return false;
			}
		}
		return true;
	}
	
	private boolean atLeastOneWordIsUppercase(String[] words) {
		for (String word: words) {
			if (isUpperCase(word)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean atLeastOneWordIsCapitalcase(String[] words) {
		for (String word: words) {
			if (isCapitalCase(word)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean fullnameContainsAtLeastMinimumWordCount(String[] words)
	{
		return (words.length >= MINIMUM_WORDS_COUNT);
	}
	
	public boolean isCapitalCase(String word) {
		char[] letters = word.toCharArray();
		if (Character.isLowerCase(letters[0])) {
			return false;
		}
					
		for (int i = 1; i < letters.length; i++) {
			if (Character.isUpperCase(letters[i])) {
				return false;
			}
		}
		return true;
	}
	
	public boolean isUpperCase(String word) {
		for(char letter : word.toCharArray()) {
			if (Character.isLowerCase(letter)) {
				return false;
			}
		}
		return true;
	}
	
	public String getLastName(String fullname) {
		String[] words = fullname.split(" ");
		
		for (String word: words) {
			if (isUpperCase(word))
			{
				return word;
			}
		}
		return null;
	}

}
