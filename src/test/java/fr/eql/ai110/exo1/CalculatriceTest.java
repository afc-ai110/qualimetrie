package fr.eql.ai110.exo1;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatriceTest {

	private Calculatrice calc;
	
	@BeforeAll
	public static void SetUp()
	{
		
	}
	
	@BeforeEach
	public void beforeEach()
	{
		calc = new Calculatrice();
	}
	
	@Test
	public void Multiplication_WhenA_XOR_B_IsZero_ResultIsZero()
	{
		int a = 0;
		int b = 5;
		
		assertEquals(0, calc.multiplier(a, b));
	}
	
	@Test
	public void Multiplication_WhenAAndBAreSameSign_ResultIsPositive()
	{
		int a = 10;
		int b = 5;
		assertTrue(calc.multiplier(a, b) > 0);
		
		a = -10;
		b = -5;
		assertTrue(calc.multiplier(a, b) > 0);
	}
		
	@Test
	public void Multiplication_When_A_XOR_B_IsNegative_ResultIsNegative()
	{
		int a = 10;
		int b = -5;
		assertTrue(calc.multiplier(a, b) < 0);
		
		a = -10;
		b = 5;
		assertTrue(calc.multiplier(a, b) < 0);	
	}
		
	@Test
	public void Addition_WhenAAndBPositive_ReturnSum()
	{
		assertEquals(42, calc.additionner(39, 3));
	}
	
	@Test
	public void Addition_WhenAPositiveAndBNegative_ShouldReturnLowerThanA()
	{
		int a = 42;
		int b = -12;
		int result = calc.additionner(a, b);
		
		assertTrue(result < a);
	}
	
	@Test
	public void Addition_WhenANegativeAndBNegative_ShouldReturnLowerThanA()
	{
		int a = -42;
		int b = -12;
		int result = calc.additionner(a, b);
		
		assertTrue(result < a);
	}
	
	
}
