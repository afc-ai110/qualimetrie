package fr.eql.ai110.exo1;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class NameValidatorTest {

	// x Fullname must be at least 2 words
	// x both orders possible (first+last OR last+first)
	// first names with several words : "Paul Henry MATHIEU"
	// last names with several words : "Juan Martin DEL POTRO"
	// x group parts : "Juan Edgardo MARTIN Del POTRO" : KO / "Juan Martin DEL POTRO" : OK
	// x Last name is uppercase
	// x Each word is either uppercase Or Capitalcase
	// x At least one uppercase word
	// x At least one Capitalcase word
	
	@Test
	public void fullname_with_MixedWords_IsIncorrect() {
		NameValidator validator = new NameValidator();
		String fullname = "DE LA BATHE PaUL HeNRY Mathieu";
		assertFalse(validator.isCorrectFullname(fullname));
	}
	
	@Test
	public void fullname_with_mixedUpperAndCapital_IsIncorrect() {
		NameValidator validator = new NameValidator();
		String fullname = "Juan Edgrado MARTIN NADAL Del POTRO";
		assertFalse(validator.isCorrectFullname(fullname));
		
		fullname = "MARTIN NADAL Juan Del POTRO";
		assertFalse(validator.isCorrectFullname(fullname));
	}
	
	@Test
	public void fullname_with_CapitalsFirst_IsCorrect() {
		NameValidator validator = new NameValidator();
		String fullname = "Paul Henry Mathieu DE LA BATHE";
		assertTrue(validator.isCorrectFullname(fullname));
	}

	@Test
	public void fullname_with_UppersFirst_IsCorrect() {
		NameValidator validator = new NameValidator();
		String fullname = "DE LA BATHE Paul Henry Mathieu";
		assertTrue(validator.isCorrectFullname(fullname));
	}

	
	@Test
	public void fullname_withNoCapitalCaseWord_IsIncorrect() {
		NameValidator validator = new NameValidator();
		String fullname = "ANDY MURRAY";
		assertFalse(validator.isCorrectFullname(fullname));
	}
	
	@Test
	public void fullname_withNoUpperCaseWord_IsIncorrect() {
		NameValidator validator = new NameValidator();
		String fullname = "Andy Murray";
		assertFalse(validator.isCorrectFullname(fullname));
	}

	@Test
	public void getLastName_WithTwoWords_ReturnsLastName() {
		NameValidator validator = new NameValidator();

		String fullname = "Serena WILLIAMS";
		assertEquals("WILLIAMS", validator.getLastName(fullname));

		fullname = "GASQUET Richard";
		assertEquals("GASQUET", validator.getLastName(fullname));
	}

	@Test
	public void fullname_WithOnlyUpperCaseFirstLetters_IsCorrect() {
		NameValidator validator = new NameValidator();
		String fullname = "Rafael NADAL";
		assertTrue(validator.isCorrectFullname(fullname));
	}

	@Test
	public void fullname_WithLowerCaseFirstLetters_IsIncorrect() {
		NameValidator validator = new NameValidator();
		String fullname = "benoit paire";
		assertFalse(validator.isCorrectFullname(fullname));
	}

	@Test
	public void fullName_WithTwoWords_IsCorrect() {
		NameValidator validator = new NameValidator();
		String fullname = "Novak DJOKOVIC";
		assertTrue(validator.isCorrectFullname(fullname));
	}

	@Test
	public void fullName_WithOnlyOneWord_IsIncorrect() {
		NameValidator validator = new NameValidator();
		String fullname = "Roger";
		assertFalse(validator.isCorrectFullname(fullname));
	}

	@Test
	public void fullName_WithThreeWords_IsCorrect() {
		NameValidator validator = new NameValidator();
		String fullname = "Jo Wilfried TSONGA";
		assertTrue(validator.isCorrectFullname(fullname));
	}

}
